# 2DCorrelation
-----------

# Installation
This package is installed via [`stack`](https://docs.haskellstack.org/en/stable/README/).
After installing the dependencies, installation becomes a simple:
```
stack install
```

## Dependencies
### Debian/Ubuntu
On Debian Buster make sure to have the `buster-backports` repository enabled.
```
sudo apt-get install \
  libgirepository1.0-dev \
  libwebkit2gtk-4.0-dev \
  libgtksourceview-3.0-dev \
  llvm-8-dev \
  libopenblas-dev \
  libfftw3-dev
```
Older versions (including Debian Buster's verion) of Gtk have an upstream bug, which prevents you from correctly linking.
For Debian a patch is supplied, which needs to be applied to the version installed by apt (I know this is dirty).
See [here](https://github.com/haskell-gi/haskell-gi/issues/212).
```
sudo patch /usr/share/gir-1.0/Gtk-3.0.gir < GtkDebian.patch
```
