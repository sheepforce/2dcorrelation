import           RIO
import           GI.Gtk.Declarative.App.Simple
import           Corr2D
import           GUI
import           Data.Attoparsec.Text


main :: IO ()
main = runSimpleApp $ do
  raw <- readFileUtf8 "data/TestData.csv"
  let inputSpectra = Homo <$> parseOnly parseSpectraInformation raw
  case inputSpectra of
    Left  err    -> logError . displayShow $ err
    Right parsed -> do
     correlated       <- correlate parsed
     -- corrResWithPlots <- plotCorrelationSpectrum correlated
     return ()
  void . liftIO . run $ myApp
