{-# LANGUAGE TemplateHaskell #-}
module Corr2D
  ( OriginalSpectra
  , CorrelationResult(..)
  , CorrelationTask(..)
  , parseSpectraInformation
  , correlate
  , plotCorrelationSpectrum
  )
where
import qualified Corr2D.Internal               as Internal
import           Data.Array.Accelerate          ( (:.)(..)
                                                , Acc
                                                , All(..)
                                                , Matrix
                                                , Scalar
                                                , Vector
                                                , Z(..)
                                                )
import qualified Data.Array.Accelerate         as Acc
import           Data.Array.Accelerate.Data.Complex
                                                ( Complex(..) )
import           Data.Array.Accelerate.IO.Data.Vector.Unboxed
                                                ( toUnboxed )
import           Data.Array.Accelerate.LLVM.Native
                                                ( runN
                                                , runQ
                                                )
import           Data.Attoparsec.Text
import           Data.Ix
import qualified Graphics.Gnuplot.Advanced     as Gnuplot
import qualified Graphics.Gnuplot.Display      as GPDisplay
import qualified Graphics.Gnuplot.Frame        as GPFrame
import qualified Graphics.Gnuplot.Frame.Option as GPOption
import qualified Graphics.Gnuplot.Frame.OptionSet
                                               as GPOptionSet
import qualified Graphics.Gnuplot.Graph.ThreeDimensional
                                               as Graph3D
import qualified Graphics.Gnuplot.Graph.TwoDimensional
                                               as Graph2D
import qualified Graphics.Gnuplot.Plot.ThreeDimensional
                                               as Plot3D
import           Graphics.Gnuplot.Terminal.PNG as GPPNG
import           Graphics.Gnuplot.Terminal.SVG as GPSVG
import           RIO                     hiding ( Vector
                                                , takeWhile
                                                )
import           RIO.Char
import qualified RIO.HashMap                   as HashMap
import qualified RIO.List                      as List
import qualified RIO.List.Partial              as List
                                                ( head )
import           RIO.Seq                        ( Seq(..) )
import qualified RIO.Seq                       as Seq
import qualified RIO.Text                      as Text
import qualified RIO.Vector.Unboxed            as VectorU
import           System.IO.Temp
import qualified Graphics.Gnuplot.MultiPlot    as Multiplot
import qualified Graphics.Gnuplot.Plot.TwoDimensional
                                               as Plot2D

{-|
The spectral information of the spectra that shall be correlated. This data type is not to be
exposed and should be constructed from safe helper functions to be kept consistent.
-}
data OriginalSpectra = OriginalSpectra
  { xValues    :: !(Vector Double)         -- ^ The shared x values of all spectra.
  , yValues    :: !(Matrix Double)         -- ^ The y-values of a series of spectra. A given column
                                           --   represents a spectrum.
  , refValues  :: !(Maybe (Vector Double)) -- ^ Possibly a reference spectrum for this set of
                                           --   spectra.
  , names      :: !(Seq Text)              -- ^ Names of the columns in the matrix 'spectra'.
  , xLabel     :: !Text                    -- ^ Label of the x-Axis of all spectra.
  }

{-|
The calculated correlation spectra.
-}
data CorrelationResult = CorrelationResult
  { zSynchronous      :: !(Matrix Double)      -- ^ The correlation matrix (z values) for
                                               --   synchronous correlation spectrum.
  , zAsynchronous     :: !(Matrix Double)      -- ^ The correlation matrix (z values) for
                                               --   asynchronous correlation spectrum.
  , xyValues          :: !(Vector Double)      -- ^ The x and y values of the surface plot (same on
                                               --   both axes).
  , xyLabel           :: !Text                 -- ^ Label for the x- and y-axis (usually annotation
                                               --   for dimension and unit).
  , originalSpectra   :: !OriginalSpectraSet   -- ^ The original yValues from the spectral sets.
  , referenceSpectrum :: !CorrelationReference -- ^ The reference spectrum to plot on the sides of
                                               --   the correlation matrix.
  , seriesPlot        :: !(Maybe (GPFrame.T (Graph3D.T Double Double Double))) -- ^ Maybe a gnuplot
                                               --   plot for the pertubation plot series.
  , syncPlot          :: !(Maybe (GPFrame.T (Graph3D.T Double Double Double))) -- ^ Maybe a gnuplot
                                               --   plot for the synchronous correlation spectrum.
  , asyncPlot         :: !(Maybe (GPFrame.T (Graph3D.T Double Double Double))) -- ^ Maybe a gnuplot
                                               --   plot for the asynchronous correlation spectrum.
  }

{-|
Perform a Homocorrelation calculation, which needs one series of spectra, or a Heterocorrelation
analysis, which needs two set of spectra.
-}
data CorrelationTask
  = Homo OriginalSpectra
  | Hetero OriginalSpectra OriginalSpectra

data OriginalSpectraSet
  = HomoOriginal !(Matrix Double)
  | HeteroOrignal !(Matrix Double, Matrix Double)

data CorrelationReference
  = HomoRef (Vector Double)
  | HeteroRef (Vector Double) (Vector Double)

data ArrayMismatchException = ArrayMismatchException String deriving Show
instance Exception ArrayMismatchException

{-|
Safe parser for spectral information. Parses a Gnuplot compatible format. The last column might have
the label "reference" and contain a given reference spectrum to use for the correlation analysis.
This last row is optional and might be omitted. The format looks like this:

@
DimensionAndOrUnit label0 label1 label2 label3 label4 ... ("reference")
xVal0              y0Val0 y1Val0 y2Val0 y3Val0 y4Val0 ... (yRVal0)
xVal1              y0Val1 y1Val1 y2Val1 y3Val1 y4Val1 ... (yRVal1)
xVal2              y0Val2 y1Val2 y2Val2 y3Val2 y4Val2 ... (yRVal2)
...                ...    ...    ...    ...    ...    ... (...)
@
-}
parseSpectraInformation :: Parser OriginalSpectra
parseSpectraInformation = do
  xLabel'     <- skipHorizontalSpace *> takeWhile (not . isSpace) <* skipHorizontalSpace
  namesAndRef <- Seq.fromList . Text.words <$> takeWhile (not . isEndOfLine) <* endOfLine

  let (names', hasReference) = case namesAndRef of
        normalNames :|> "reference" -> (normalNames, True)
        normalNames                 -> (normalNames, False)
      nColumns = Seq.length namesAndRef + 1

  valListMat <- many1 (count nColumns (skipHorizontalSpace *> double) <* endOfLine)
  let nRows       = List.length valListMat
      plainValMat = Acc.fromList (Z :. nRows :. nColumns) . List.concat $ valListMat
      xValues'    = matSliceColumn plainValMat (Acc.fromList Z [0])
      (yValues', refValues') =
        let startIndY = Acc.fromList Z [1]
            refInd    = Acc.fromList Z [nColumns - 1]
            nYColumns = Acc.fromList Z [Seq.length names']
        in  if hasReference
              then
                (matSlit plainValMat startIndY nYColumns, Just $ matSliceColumn plainValMat refInd)
              else (matSlit plainValMat startIndY nYColumns, Nothing)
  return OriginalSpectra { xValues   = xValues'
                         , yValues   = yValues'
                         , refValues = refValues'
                         , names     = names'
                         , xLabel    = xLabel'
                         }
 where
  skipHorizontalSpace :: Parser ()
  skipHorizontalSpace = do
    _ <- takeWhile (`elem` [' ', '\t', '\f', '\v'])
    return ()
  matSliceColumn :: Matrix Double -> Scalar Int -> Vector Double
  matSliceColumn = $( runQ Internal.matSliceColumn' )
  matSlit :: Matrix Double -> Scalar Int -> Scalar Int -> Matrix Double
  matSlit = $( runQ Internal.matSlit' )

{-|
Correlate a set of spectra. Accepts both Homo- and Heterocorrelation data sets.
-}
correlate :: MonadThrow m => CorrelationTask -> m CorrelationResult
correlate taskAndInfo = case taskAndInfo of
  Homo singleSet -> do
    let undynamicSpectra = yValues singleSet
        refSpectrum      = fromMaybe (averageSpectra undynamicSpectra) (refValues singleSet)
        corrMatrix       = correlate' undynamicSpectra refSpectrum undynamicSpectra refSpectrum
    return CorrelationResult { zSynchronous      = arrReal corrMatrix
                             , zAsynchronous     = arrImag corrMatrix
                             , xyValues          = xValues singleSet
                             , xyLabel           = xLabel singleSet
                             , originalSpectra   = HomoOriginal . yValues $ singleSet
                             , referenceSpectrum = HomoRef refSpectrum
                             , seriesPlot        = Nothing
                             , syncPlot          = Nothing
                             , asyncPlot         = Nothing
                             }
  Hetero setA setB -> do
    let undynamicSpectraA          = yValues setA
        undynamicSpectraB          = yValues setB
        Z :. nValuesA :. nSpectraA = Acc.arrayShape undynamicSpectraA
        Z :. nValuesB :. nSpectraB = Acc.arrayShape undynamicSpectraB

    unless (nValuesA == nValuesB && nSpectraA == nSpectraB && (xValues setA == xValues setB))
      . throwM
      $ ArrayMismatchException
          "The number of data points or the number of spectra does not match between the two data\
          \ sets for heterocorrelation."

    let refSpectrumA = fromMaybe (averageSpectra undynamicSpectraA) (refValues setA)
        refSpectrumB = fromMaybe (averageSpectra undynamicSpectraB) (refValues setB)
        corrMatrix   = correlate' undynamicSpectraA refSpectrumA undynamicSpectraB refSpectrumB
    return CorrelationResult { zSynchronous      = arrReal corrMatrix
                             , zAsynchronous     = arrImag corrMatrix
                             , xyValues          = xValues setA
                             , xyLabel           = xLabel setA
                             , originalSpectra   = HeteroOrignal (yValues setA, yValues setB)
                             , referenceSpectrum = HeteroRef refSpectrumA refSpectrumB
                             , seriesPlot        = Nothing
                             , syncPlot          = Nothing
                             , asyncPlot         = Nothing
                             }
 where
  correlate'
    :: Matrix Double -> Vector Double -> Matrix Double -> Vector Double -> Matrix (Complex Double)
  correlate' = runN Internal.correlate'
  averageSpectra :: Matrix Double -> Vector Double
  averageSpectra = $( runQ Internal.averageSpectra' )
  arrReal :: Matrix (Complex Double) -> Matrix Double
  arrReal = $( runQ Internal.arrReal' )
  arrImag :: Matrix (Complex Double) -> Matrix Double
  arrImag = $( runQ Internal.arrImag' )

{-|
Creates Gnuplot scripts to plot the sepctral series, the synchronous and the asynchronous
correlation  spectrum.
-}
plotCorrelationSpectrum :: CorrelationResult -> CorrelationResult
plotCorrelationSpectrum corrRes =
  {-
  tmpDir                      <- liftIO getCanonicalTemporaryDirectory
  (syncTmpFile , syncHandle ) <- liftIO $ openTempFile tmpDir "sync"
  (asyncTmpFile, asyncHandle) <- liftIO $ openTempFile tmpDir "async"
  liftIO . hClose $ syncHandle
  liftIO . hClose $ asyncHandle
  -}

  let assocSynchronousZValues  = zSynchronous corrRes
      assocAsynchronousZValues = zAsynchronous corrRes
      xyValueList              = Acc.toList . xyValues $ corrRes
      xyValueToIxMap           = HashMap.fromList . Acc.toList . assocVecWithIx . xyValues $ corrRes
      syncMin                  = minimumMat assocSynchronousZValues
      syncMax                  = maximumMat assocSynchronousZValues
      syncAbsRange             = max (abs syncMin) (abs syncMax)
      asyncMin                 = minimumMat assocAsynchronousZValues
      asyncMax                 = maximumMat assocAsynchronousZValues
      asyncAbsRange            = max (abs asyncMin) (abs asyncMax)
      axesLabel                = Text.unpack . xyLabel $ corrRes
      synchronousPlot :: GPFrame.T (Graph3D.T Double Double Double)
      synchronousPlot =
          GPFrame.cons
              ( GPOptionSet.add (GPOption.custom "palette" "palette")
                                ["defined", "(-1 0 0 1, -0.5 0 1 1, 0 1 1 1, 0.5 1 1 0, 1 1 0 0)"]
              $ GPOptionSet.add
                  (GPOption.custom "cbrange" "cbrange")
                  ["[" <> show (-1 * syncAbsRange) <> ":" <> show syncAbsRange <> "]"]
              $ GPOptionSet.zRange3d (-1 * syncAbsRange, syncAbsRange)
              $ GPOptionSet.key False
              $ GPOptionSet.title ""
              $ GPOptionSet.viewMap
              $ GPOptionSet.yLabel axesLabel
              $ GPOptionSet.xLabel axesLabel
              $ GPOptionSet.sizeSquare
              $ GPOptionSet.deflt
              )
            $ Plot3D.surface xyValueList xyValueList (xyToZVal xyValueToIxMap assocSynchronousZValues)
      asynchronousPlot :: GPFrame.T (Graph3D.T Double Double Double)
      asynchronousPlot =
          GPFrame.cons
              ( GPOptionSet.add (GPOption.custom "palette" "palette")
                                ["defined", "(-1 0 0 1, -0.5 0 1 1, 0 1 1 1, 0.5 1 1 0, 1 1 0 0)"]
              $ GPOptionSet.add
                  (GPOption.custom "cbrange" "cbrange")
                  ["[" <> show (-1 * asyncAbsRange) <> ":" <> show asyncAbsRange <> "]"]
              $ GPOptionSet.zRange3d (-1 * asyncAbsRange, asyncAbsRange)
              $ GPOptionSet.key False
              $ GPOptionSet.title ""
              $ GPOptionSet.viewMap
              $ GPOptionSet.yLabel axesLabel
              $ GPOptionSet.xLabel axesLabel
              $ GPOptionSet.sizeSquare
              $ GPOptionSet.deflt
              )
            $ Plot3D.surface
                xyValueList
                xyValueList
                (xyToZVal xyValueToIxMap assocAsynchronousZValues)
  in  (corrRes { syncPlot = Just synchronousPlot, asyncPlot = Just asynchronousPlot })
  {-
  exitCodeSync  <- liftIO $ Gnuplot.plot (GPPNG.cons syncTmpFile) synchronousPlot
  exitCodeAsync <- liftIO $ Gnuplot.plot (GPPNG.cons asyncTmpFile) asynchronousPlot
  case (exitCodeSync, exitCodeAsync) of
    (ExitSuccess, ExitSuccess) -> return (syncTmpFile, asyncTmpFile)
    (ExitFailure _, _) ->
      fail "Got non-0 exit code from Gnuplot while plotting synchronous spectrum."
    (_, ExitFailure _) ->
      fail "Got non-0 exit code from Gnuplot while plotting asynchronous spectrum."
  -}
 where
  assocVecWithIx :: Vector Double -> Vector (Double, Int)
  assocVecWithIx = $( runQ Internal.assocVecWithIx' )
  xyToZVal :: HashMap Double Int -> Matrix Double -> Double -> Double -> Double
  xyToZVal lookupMap valueMat xVal yVal =
    let ixR = HashMap.lookupDefault (-1) xVal lookupMap
        ixC = HashMap.lookupDefault (-1) yVal lookupMap
    in  valueMat `Acc.indexArray` (Z :. ixR :. ixC)
  xToYVal :: HashMap Double Int -> Vector Double -> Double -> Double
  xToYVal lookupMap valueVec xVal =
    let ix = HashMap.lookupDefault (-1) xVal lookupMap in valueVec `Acc.indexArray` (Z :. ix)
  minimumMat :: Matrix Double -> Double
  minimumMat mat = List.head . Acc.toList . minimumMat' $ mat
    where minimumMat' = $( runQ Internal.minimumMat' )
  maximumMat :: Matrix Double -> Double
  maximumMat mat = List.head . Acc.toList . maximumMat' $ mat
    where maximumMat' = $( runQ Internal.maximumMat' )
