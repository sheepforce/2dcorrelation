module Corr2D.Internal
  ( averageSpectra'
  , correlate'
  , arrReal'
  , arrImag'
  , matSliceRow'
  , matSliceColumn'
  , matSlit'
  , assocMatWithIx'
  , assocVecWithIx'
  , xyDistances'
  , minDistance'
  , minimumMat'
  , maximumMat'
  , linSpace
  )
where
import           Data.Array.Accelerate          ( (:.)(..)
                                                , Acc
                                                , Scalar
                                                , Matrix
                                                , Vector
                                                , Z(..)
                                                , Exp
                                                , All(..)
                                                , Shape
                                                , Array
                                                , Elt
                                                )
import qualified Data.Array.Accelerate         as Acc
import           RIO                     hiding ( Vector )
import qualified Data.Array.Accelerate.Data.Complex
                                               as Acc
import           Data.Array.Accelerate.Data.Complex
                                                ( Complex(..) )
import qualified Data.Array.Accelerate.Numeric.LinearAlgebra
                                               as Acc
import qualified Data.Array.Accelerate.Math.FFT
                                               as Acc

{-|
Average a matrix of spectra, where the individual spectra are represented as columns of y-Values.
-}
averageSpectra' :: Acc (Matrix Double) -> Acc (Vector Double)
averageSpectra' specMat =
  let (Z :. _nRows :. nCols) = Acc.unlift . Acc.shape $ specMat :: (Z :. Exp Int :. Exp Int)
  in  Acc.map (/ Acc.fromIntegral nCols) . Acc.sum $ specMat

{-|
Convert a real valued array to its formally complex form.
-}
toComplex :: Shape sh => Acc (Array sh Double) -> Acc (Array sh (Complex Double))
toComplex arr = Acc.map toComplexExp arr
 where
  toComplexExp :: Exp Double -> Exp (Complex Double)
  toComplexExp = Acc.lift1 (:+ (0 :: Exp Double))

{-|
From a complex valued array, only use the real part.
-}
fromComplex :: Shape sh => Acc (Array sh (Complex Double)) -> Acc (Array sh Double)
fromComplex = Acc.map Acc.real

{-|
Generic function to calculate the correlation matrix of both homo-correlation and hetero-correlation
spectra. In case of homo-correlation setA == setB and refSpecA == refSpecB.

The function expects the sets of input spectra in a representation, where each column represents a
spectrum.

The spectra sets  will be transposed after they the fourier transformation has been performed
(Accelerate does not allow to choose the axis along which to do the transformation and uses the
innermost axis instead). The transponation after fourier transformation brings them in the form used
in literature, where each row is a spectrum.

__This function trusts its input data to a very high degree! Make sure to only supply proper data.__
-}
correlate'
  :: Acc (Matrix Double)          -- ^ Set A of spectra with each column representing a spectrum.
  -> Acc (Vector Double)          -- ^ Reference spectrum for set A.
  -> Acc (Matrix Double)          -- ^ Set B of spectra with each column representing a spectrum.
  -> Acc (Vector Double)          -- ^ Reference spectrum for set B.
  -> Acc (Matrix (Complex Double))
correlate' specMatA refSpecA specMatB refSpecB =
  let (Z :. _nValsA :. nSpectraA) = Acc.unlift . Acc.shape $ specMatA :: Z :. Exp Int :. Exp Int
      (Z :. _nValsB :. nSpectraB) = Acc.unlift . Acc.shape $ specMatB :: Z :. Exp Int :. Exp Int
      refMatA                     = Acc.replicate (Acc.lift $ Z :. All :. nSpectraA) refSpecA
      refMatB                     = Acc.replicate (Acc.lift $ Z :. All :. nSpectraB) refSpecB
      dynamicA                    = Acc.zipWith (-) specMatA refMatA
      dynamicB                    = Acc.zipWith (-) specMatB refMatB
      fourierA                    = Acc.transpose . Acc.fft Acc.Forward . toComplex $ dynamicA
      fourierB                    = Acc.transpose . Acc.fft Acc.Forward . toComplex $ dynamicB
      adjointFourierB             = Acc.transpose . Acc.map Acc.conjugate $ fourierB
  in  adjointFourierB Acc.<> fourierA

{-|
Real part of all values of a complex-valued array.
-}
arrReal' :: Acc (Matrix (Complex Double)) -> Acc (Matrix Double)
arrReal' = Acc.map Acc.real

{-|
Imaginary part of all values of a complex-valued array.
-}
arrImag' :: Acc (Matrix (Complex Double)) -> Acc (Matrix Double)
arrImag' = Acc.map Acc.imag

{-|
Specialised slice: get a row from a matrix.
-}
matSliceRow' :: Elt a => Acc (Matrix a) -> Acc (Scalar Int) -> Acc (Vector a)
matSliceRow' mat row = Acc.slice mat (Acc.lift $ Z :. Acc.the row :. All)

{-|
Specialised slice: get a column from a matrix.
-}
matSliceColumn' :: Acc (Matrix Double) -> Acc (Scalar Int) -> Acc (Vector Double)
matSliceColumn' mat col = Acc.slice mat (Acc.lift $ Z :. All :. Acc.the col)

{-|
Specialised take for matrices.
-}
matSlit' :: Acc (Matrix Double) -> Acc (Scalar Int) -> Acc (Scalar Int) -> Acc (Matrix Double)
matSlit' mat startInd nColumns = Acc.slit (Acc.the startInd) (Acc.the nColumns) mat

{-|
Associate all elements of a matrix with its indices.
-}
assocMatWithIx' :: Acc (Matrix Double) -> Acc (Matrix (Int, Int, Double))
assocMatWithIx' mat = Acc.imap
  (\ix el ->
    let Z :. ixR :. ixC = Acc.unlift ix :: Z :. Exp Int :. Exp Int in Acc.lift (ixR, ixC, el)
  )
  mat

{-|
Associate all elements of a vector with its indices.
-}
assocVecWithIx' :: Acc (Vector Double) -> Acc (Vector (Double, Int))
assocVecWithIx' vec =
  Acc.imap (\ix el -> let Z :. ixV = Acc.unlift ix :: Z :. Exp Int in Acc.lift (el, ixV)) vec

{-|
Calculate the disance between neighbouring points in an array. Assumes that the vector is ordered.
-}
xyDistances' :: Acc (Vector Double) -> Acc (Vector Double)
xyDistances' vec =
  let nElems = Acc.length vec in Acc.take (nElems - 1) $ Acc.stencil stencil3 Acc.clamp vec
 where
  stencil3 :: Acc.Stencil3 Double -> Exp Double
  stencil3 (_, c, r) = abs $ r - c

{-|
Get the minimum distance between elements of an ordered vector.
-}
minDistance' :: Acc (Vector Double) -> Acc (Scalar Double)
minDistance' vec = Acc.minimum . xyDistances' $ vec

{-|
Get the minimum value of the Matrix.
-}
minimumMat' :: Acc (Matrix Double) -> Acc (Scalar Double)
minimumMat' mat = Acc.minimum . Acc.flatten $ mat

{-|
Get the maximum value of the Matrix.
-}
maximumMat' :: Acc (Matrix Double) -> Acc (Scalar Double)
maximumMat' mat = Acc.maximum . Acc.flatten $ mat

{-|
Generate a linear spaced range from lower to upper bounds.
-}
linSpace :: Int -> (Double, Double) -> [Double]
linSpace n (minVal, maxVal) =
  let stepWidth = (abs $ maxVal - minVal) / fromIntegral (n - 1)
  in  [ minVal + (fromIntegral i * stepWidth) | i <- [0 .. n - 1] ]
