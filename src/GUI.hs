{-# LANGUAGE OverloadedLabels #-}
{-# LANGUAGE OverloadedLists  #-}
module GUI
  ( myApp
  )
where
import           GI.Gtk                         ( Button(..)
                                                , FileChooserButton(..)
                                                , Image(..)
                                                , Label(..)
                                                , ListBox(..)
                                                , ListBoxRow(..)
                                                , Window(..)
                                                , ToggleButton(..)
                                                , MessageDialog(..)
                                                , fileChooserGetFilename
                                                , toggleButtonGetActive
                                                )
import           GI.Gtk.Declarative
import           GI.Gtk.Declarative.App.Simple
import           RIO                     hiding ( on
                                                , view
                                                , (^.)
                                                , first
                                                , hClose
                                                )
import qualified RIO.Text                      as Text
import qualified Corr2D
import           Data.Attoparsec.Text
import           Data.Bifunctor
import qualified Graphics.Gnuplot.Advanced     as Gnuplot
import qualified Graphics.Gnuplot.Terminal.PNG as GnuplotPNG
import           System.IO.Temp
import           System.IO                      ( hClose )

{-|
State of the GUI.
-}
data GUIState = GUIState
  { correlationMode :: CorrelationMode
  , plots           :: Plots
  , results :: Maybe Corr2D.CorrelationResult
  , showError :: Maybe Text
  }

{-|
Select if a hetero- or homocorrelation spectrum should be obtained. Input files can be specified.
-}
data CorrelationMode = Homo (Maybe FilePath) | Hetero (Maybe FilePath, Maybe FilePath)


{-|
The results of the calculation. All plots are optional to indicate, that they are not available from
the beginning.
-}
data Plots = Plots
  { spectraSeries :: Maybe FilePath -- ^ Normal 1D spectra plot.
  , synchronous   :: Maybe FilePath -- ^ Synchronous 2D correlation spectrum.
  , asynchronous  :: Maybe FilePath -- ^ Asynchronous 2D correlation spectrum.
  }

data PlotData = PlotData
  {
  }

----------------------------------------------------------------------------------------------------
{-|
Events of the UserInterface
-}
data GUIEvent
  = HomoCorrelationSelected        -- ^ The button for homo correlation has been clicked..
  | HeteroCorrelationSelected      -- ^ The button for heterocorrelation has been clicked.
  | HomoFileChosen (Maybe FilePath)
  | HeteroFile1Chosen (Maybe FilePath)
  | HeteroFile2Chosen (Maybe FilePath)
  | StartButtonClicked             -- ^ The button to start the calculation has been clicked.
  | StartEventFinished Plots (Maybe Corr2D.CorrelationResult)
  | SaveButtonClicked              -- ^ The button to save the plots and data has been clicked.
  | ErrorEvent Text
  | Closed                         -- ^ The window has been closed.

----------------------------------------------------------------------------------------------------
{-|
The 'view' function of the GTK AppSimple system, drawing the window to the screen.
-}
view' :: GUIState -> AppView Window GUIEvent
view' state = bin
  Window
  [ #title := "Corr2D"
  , #widthRequest := 800
  , #heightRequest := 600
  , on #deleteEvent (const (True, Closed))
  ]
  windowsSplit
 where
  -- Split the window to a left and a right part.
  windowsSplit = paned
    [#wideHandle := True]
    (pane defaultPaneProperties spectra)
    (  pane defaultPaneProperties
    $  container ListBox []
    $  [ bin ListBoxRow [] homoHeteroSwitch
       , bin ListBoxRow [] fileSelection
       , bin ListBoxRow [] startButton
       , bin ListBoxRow [] saveButton
       ]
    -- <> case showError state of
    --      Nothing  -> mempty
    --      Just err -> [bin ListBoxRow [] $ widget MessageDialog [#text := err]]
    )

  -- Tab selection for the spectra.
  spectra = notebook
    [#widthRequest := 500]
    [ page "Pertubation Series" pertubationPlot
    , page "Synchronous"        synchronousPlot
    , page "Asynchronous"       asynchronousPlot
    ]
   where
    pertubationPlot = case spectraSeries . plots $ state of
      Nothing  -> widget Label [#label := "Pertubation series"]
      Just img -> widget Image [#file := Text.pack img]
    synchronousPlot = case synchronous . plots $ state of
      Nothing  -> widget Label [#label := "Synchronous spectrum"]
      Just img -> widget Image [#file := Text.pack img]
    asynchronousPlot = case asynchronous . plots $ state of
      Nothing  -> widget Label [#label := "Asynchronous spectrum"]
      Just img -> widget Image [#file := Text.pack img]

  -- Selection buttons for homo or heterocorrelation.
  homoHeteroSwitch = widget
    ToggleButton
    [ onM
        #toggled
        (fmap (\isHetero -> if isHetero then HeteroCorrelationSelected else HomoCorrelationSelected)
        . toggleButtonGetActive
        )
    ]
    {-
    paned
    []
    ( pane defaultPaneProperties
    $ widget Button [#label := "Homo", on #clicked HomoCorrelationSelected]
    )
    ( pane defaultPaneProperties
    $ widget Button [#label := "Hetero", on #clicked HeteroCorrelationSelected]
    )
    -}

  -- File chooser to select a spectrum of interest.
  fileSelection = case correlationMode state of
    Homo _ -> widget
      FileChooserButton
      [ #title := "Spectrum Data File"
      , onM #selectionChanged (fmap HomoFileChosen . fileChooserGetFilename)
      ]
    Hetero _ -> paned
      []
      (pane defaultPaneProperties $ widget
        FileChooserButton
        [ #title := "Set A spectra"
        , onM #selectionChanged (fmap HeteroFile1Chosen . fileChooserGetFilename)
        ]
      )
      (pane defaultPaneProperties $ widget
        FileChooserButton
        [ #title := "Set B spectra"
        , onM #selectionChanged (fmap HeteroFile2Chosen . fileChooserGetFilename)
        ]
      )

  -- Button to start the calculation. Will read the file an start doing calculations.
  startButton = widget Button [#label := "Start calculation ...", on #clicked StartButtonClicked]

  -- Button to start the calculation. Will save the 3 graphs to current directory and write a
  -- gnuplot-suitable file format.
  saveButton  = widget Button [#label := "Save plots and data ...", on #clicked SaveButtonClicked]

----------------------------------------------------------------------------------------------------
{-|
The 'update' function of the GTK AppSimple system, taking care of making a transition.
-}
update' :: GUIState -> GUIEvent -> Transition GUIState GUIEvent
update' state event = case event of
  HomoCorrelationSelected -> Transition (state { correlationMode = Homo Nothing }) noEvent
  HeteroCorrelationSelected ->
    Transition (state { correlationMode = Hetero (Nothing, Nothing) }) noEvent
  HomoFileChosen    f -> Transition (state { correlationMode = Homo f }) noEvent
  HeteroFile1Chosen f -> case correlationMode state of
    Hetero (_, f2) -> Transition (state { correlationMode = Hetero (f, f2) }) noEvent
    Homo   _       -> Transition (state { correlationMode = Hetero (f, Nothing) }) noEvent
  HeteroFile2Chosen f -> case correlationMode state of
    Hetero (f1, _) -> Transition (state { correlationMode = Hetero (f1, f) }) noEvent
    Homo   _       -> Transition (state { correlationMode = Hetero (Nothing, f) }) noEvent
  StartButtonClicked -> Transition state (startEventHandler state) -- TODO
  StartEventFinished newPlots corrRes ->
    Transition (state { plots = newPlots, results = corrRes }) noEvent
  SaveButtonClicked -> Transition state noEvent -- TODO
  ErrorEvent errMsg -> Transition (state { showError = Just errMsg }) noEvent
  Closed            -> Exit
  where noEvent = return Nothing

{-|
Handler to deal with a pressed start button. Performs the calculation and does the plots. Triggers a
new event, which either contains information to update the state with the results or an error event.
-}
startEventHandler :: GUIState -> IO (Maybe GUIEvent)
startEventHandler state = runSimpleApp $ do
  logInfo "Processing files."
  correlationResult <- case correlationMode state of
    Homo (Just file) -> do
      raw <- readFileUtf8 file
      let parsed = first (exceptionToEvent "Parsing the input data failed:\n")
            $ parseOnly Corr2D.parseSpectraInformation raw
          correlated =
            parsed
              >>= (first (exceptionToEvent "Correlating the data set failed:\n") . Corr2D.correlate)
              .   Corr2D.Homo
      return correlated

    Hetero (Just file1, Just file2) -> do
      raw1 <- readFileUtf8 file1
      raw2 <- readFileUtf8 file2
      let parsed =
            sequence
            . fmap
                ( first (exceptionToEvent "Parsing the input data failed:\n")
                . parseOnly Corr2D.parseSpectraInformation
                )
            $ [raw1, raw2] :: Either (Maybe GUIEvent) [Corr2D.OriginalSpectra]
      let
        correlated = case parsed of
          Left errEv -> Left $ errEv
          Right [setA, setB] ->
            (first (exceptionToEvent "Correlating the data set failed:\n") . Corr2D.correlate)
              $ Corr2D.Hetero setA setB
          Right _ ->
            Left
              . Just
              . ErrorEvent
              $ "Got wrong number of spectral sets. This should never happen."

      return correlated

    _ -> return . Left . Just . ErrorEvent $ "Input files have not yet been selected."

  let gnuplotted = Corr2D.plotCorrelationSpectrum <$> correlationResult

  case gnuplotted of
    Left errEv -> return errEv

    Right corrRes@Corr2D.CorrelationResult { Corr2D.seriesPlot = Just seriesPlot, Corr2D.syncPlot = Just syncPlot, Corr2D.asyncPlot = Just asyncPlot }
      -> do
        tmpDir                   <- liftIO getCanonicalTemporaryDirectory
        (seriesTmpPath, seriesH) <- liftIO $ openTempFile tmpDir "series"
        (syncTmpPath  , syncH  ) <- liftIO $ openTempFile tmpDir "sync"
        (asyncTmpPath , asyncH ) <- liftIO $ openTempFile tmpDir "async"
        liftIO . hClose $ seriesH
        liftIO . hClose $ syncH
        liftIO . hClose $ asyncH
        _ <- liftIO . Gnuplot.plot (GnuplotPNG.cons seriesTmpPath) $ syncPlot
        _ <- liftIO . Gnuplot.plot (GnuplotPNG.cons syncTmpPath) $ asyncPlot
        _ <- liftIO . Gnuplot.plot (GnuplotPNG.cons asyncTmpPath) $ seriesPlot
        let newPlots = Plots { spectraSeries = Just seriesTmpPath
                             , synchronous   = Just syncTmpPath
                             , asynchronous  = Just asyncTmpPath
                             }
        return . Just $ StartEventFinished newPlots (Just corrRes)

    Right _ -> return . Just . ErrorEvent $ "The plotting results are incomplete."
  where exceptionToEvent msg exc = Just . ErrorEvent $ msg <> (Text.pack . show $ exc)


----------------------------------------------------------------------------------------------------
{-|
Some very empty initial state of the the GUI with nothing selected and calculated.
-}
initialState' :: GUIState
initialState' = GUIState
  { correlationMode = Homo Nothing
  , plots = Plots { spectraSeries = Nothing, synchronous = Nothing, asynchronous = Nothing }
  , results         = Nothing
  , showError       = Nothing
  }

----------------------------------------------------------------------------------------------------
{-|
Definition of the GUI.
-}
myApp :: App Window GUIState GUIEvent
myApp = App { view = view', update = update', inputs = [], initialState = initialState' }
